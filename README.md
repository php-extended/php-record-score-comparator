# php-extended/php-record-score-comparator
A library that provides record comparators from scoring values.

![coverage](https://gitlab.com/php-extended/php-record-score-comparator/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-record-score-comparator/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-mtg/php-record-score-comparator ^8`


## Basic Usage

For the basic functions, use :

```php

use PhpExtended\Record\ScoreRecordComparator;
use PhpExtended\Record\ScoreRecordComparatorDefinition;

/* @var $scoreFactory \PhpExtended\Score\ScoreFactoryInterface */
/* @var $record1 \PhpExtended\Record\RecordInterface */
/* @var $record2 \PhpExtended\Record\RecordInterface */

$definitions = new ArrayIterator([
	new ScoreRecordComparatorDefinition('name', 'name', $scoreFactory, 1),
]);

$comparator = new ScoreRecordComparator();

$distance = $comparator->compare($record1, $record2);
// the lower the distance, the more equal the objects are

```

For the packages that are useful at getting http requests (HttpProcessor),
see the `require-dev` of the `composer.json` file.


## License

MIT (See [license file](LICENSE)).
