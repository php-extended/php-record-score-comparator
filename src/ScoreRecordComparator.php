<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-score-comparator library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use InvalidArgumentException;
use Iterator;

/**
 * ScoreRecordComparator class file.
 * 
 * This class is a record comparator based on a scoring system to order the
 * records.
 * 
 * @author Anastaszor
 */
class ScoreRecordComparator implements RecordComparatorInterface
{
	
	/**
	 * The comparator definitions.
	 * 
	 * @var array<integer, ScoreRecordComparatorDefinition>
	 */
	protected array $_definitions = [];
	
	/**
	 * Builds a new ScoreRecordComparator with the given definitions.
	 * 
	 * @param Iterator<ScoreRecordComparatorDefinition> $definitions
	 */
	public function __construct(Iterator $definitions)
	{
		foreach($definitions as $definition)
		{
			$this->addDefinition($definition);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordComparatorInterface::compare()
	 * @throws InvalidArgumentException
	 */
	public function compare(RecordInterface $record1, RecordInterface $record2) : float
	{
		$points = 0.0;
		
		foreach($this->_definitions as $definition)
		{
			$value1 = $record1->getValue($definition->getSourceFieldName());
			$value2 = $record2->getValue($definition->getTargetFieldName());
			$scoreFactory = $definition->getScoreFactory();
			$score = $scoreFactory->createScore([$value1, $value2]);
			$points += $definition->getMultiplier() * (1.0 - $score->getNormalizedValue());
		}
		
		return $points;
	}
	
	/**
	 * Adds a definition.
	 * 
	 * @param ScoreRecordComparatorDefinition $definition
	 */
	protected function addDefinition(ScoreRecordComparatorDefinition $definition) : void
	{
		$this->_definitions[] = $definition;
	}
	
}
