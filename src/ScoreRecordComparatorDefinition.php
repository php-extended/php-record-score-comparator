<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-score-comparator library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use PhpExtended\Score\ScoreFactoryInterface;
use Stringable;

/**
 * ScoreRecordComparatorDefinition class file.
 * 
 * This class represents the definition for a comparison in a score-based
 * record comparator.
 * 
 * @author Anastaszor
 */
class ScoreRecordComparatorDefinition implements Stringable
{
	
	/**
	 * The source field name.
	 * 
	 * @var string
	 */
	protected string $_sourceFieldName;
	
	/**
	 * The target field name.
	 * 
	 * @var string
	 */
	protected string $_targetFieldName;
	
	/**
	 * The score factory.
	 * 
	 * @var ScoreFactoryInterface
	 */
	protected ScoreFactoryInterface $_scoreFactory;
	
	/**
	 * The multiplier.
	 * 
	 * @var float
	 */
	protected float $_multiplier = 1.0;
	
	/**
	 * Builds a new ScoreRecordComparatorDefinition with the given source and
	 * target names, score factory and multiplier.
	 * 
	 * @param string $sourceFieldName
	 * @param string $targetFieldName
	 * @param ScoreFactoryInterface $scoreFactory
	 * @param float $multiplier
	 */
	public function __construct(string $sourceFieldName, string $targetFieldName, ScoreFactoryInterface $scoreFactory, float $multiplier = 1.0)
	{
		$this->_sourceFieldName = $sourceFieldName;
		$this->_targetFieldName = $targetFieldName;
		$this->_scoreFactory = $scoreFactory;
		$this->_multiplier = $multiplier;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Gets the source field name.
	 * 
	 * @return string
	 */
	public function getSourceFieldName() : string
	{
		return $this->_sourceFieldName;
	}
	
	/**
	 * Gets the target field name.
	 * 
	 * @return string
	 */
	public function getTargetFieldName() : string
	{
		return $this->_targetFieldName;
	}
	
	/**
	 * Gets the score factory.
	 * 
	 * @return ScoreFactoryInterface
	 */
	public function getScoreFactory() : ScoreFactoryInterface
	{
		return $this->_scoreFactory;
	}
	
	/**
	 * Gets the multiplier.
	 * 
	 * @return float
	 */
	public function getMultiplier() : float
	{
		return $this->_multiplier;
	}
	
}
